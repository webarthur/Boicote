const purifix = require('purifix')
const assert = require('assertjs')
const ObjectID = require('mongodb').ObjectID
const Comments = require('../app/Comments.js')
const Posts = require('../app/Posts.js')
const Users = require('../app/Users.js')
const Mail = require('../app/Mail.js')
const Supporters = require('../app/Supporters.js')
const getDomain = function (url) {
  var matches = url.match(/^https?\:\/\/(?!(?:www\.)?(?:youtube\.com|youtu\.be))([^\/:?#]+)(?:[\/:?#]|$)/i)
  return matches && matches[1]
}

module.exports = {

  analyse (req, res) {
    const source = purifix.string(req.body.source).trim()

    assert({source})
    .the('source').isNotEmpty().or('Você precisa indicar uma fonte.')
    .the('source').isURL().or('A fonte que você indicou não parece ser um link válido.')
    if (assert.hasError) return res.fail(assert.error, 'source')

    const MetaInspector = require('node-metainspector')
    const client = new MetaInspector(source, { timeout: 5000 })

    client.on('fetch', () => {
      var dados = {
        title: client.ogTitle || client.title || '',
        text: client.ogDescription || client.description || '',
        image: client.image || ''
        // url: client.url || '',
        // scheme: client.scheme || '',
        // host: client.host || '',
        // rootUrl: client.rootUrl,
        // links: client.links,
        // author: client.author || '',
        // keywords: client.keywords,
        // charset: client.charset,
        // images: client.images,
        // feeds: client.feeds,
        // ogTitle: client.ogTitle || '',
        // ogDescription: client.ogDescription || '',
        // ogType: client.ogType || '',
        // ogUpdatedTime: client.ogUpdatedTime || '',
        // ogLocale: client.ogLocale || ''
      }

      res.json({dados: dados})
    })

    client.on('error', () => {
      res.json({erro: 1})
    })

    client.fetch()
  },

  async get (req, res) {
    const source_id = purifix.objectId(req.params.source_id)
    // valida
    assert({source_id}).the('source_id').isNotEmpty().or('Você precisa indicar uma fonte.')
    if (assert.hasError) return res.fail(assert.error, assert.inputName)

    const posts = await Posts.aggregate([
      {$match: { _id: new ObjectID(source_id) }},
      {$lookup: {
        'from': 'users',
        'localField': 'author_id',
        'foreignField': '_id',
        'as': 'author'
      }},
      {$unwind: '$author'},
      {$lookup: {
        'from': 'votes',
        'localField': '_id',
        'foreignField': 'post_id',
        'as': 'votes'
      }},
      {$lookup: {
        'from': 'comments',
        'localField': '_id',
        'foreignField': 'post_id',
        'as': 'comments'
      }},
      {$project: {
        title: 1,
        source: 1,
        text: 1,
        votes: 1,
        'author._id': 1,
        'author.nick': 1,
        'author.nome': 1,
        'author.avatar': 1,
        'num_comments': {$size: '$comments'},
        created_at: 1,
        updated_at: 1
      }}
    ])
    if (Posts.hasError) return res.fail('Ocorreu um erro no sistema.')

    // nenhum post encontrado
    if (!posts[0]) return res.fail('Página não encontrada.')

    // retorna
    res.json(posts[0])
  },

  async save (req, res) {
    const author_id = req.user._id
    const source_id = purifix.objectId(req.body.source_id)
    const comment = purifix.string(req.body.comment)
    const data = {
      parent_id: purifix.objectId(req.body.parent_id),
      source: purifix.string(req.body.source), // TODO purifix.url
      domain: getDomain(purifix.string(req.body.source)),
      type: 'reasons',
      title: purifix.string(req.body.title),
      text: purifix.string(req.body.text),
    }

    // valida
    assert(data)
    .the('title').isNotEmpty().or('A fonte precisa ter um título.')
    .the('source').isNotEmpty().or('Você precisa indicar uma fonte.')
    .the('source').isURL().or('A fonte que você indicou não parece ser um link válido.')
    .the('text').isNotEmpty().or('A fonte precisa ter um resumo.')
    .the('text').isMinLength(100).or('Defina melhor o resumo da fonte, o texto está muito curto.')
    .the('text').isMaxLength(1000).or('O texto está muito grande. Lembre-se que isto é apenas um resumo.')
    if (assert.hasError) return res.fail(assert.error, assert.inputName)

    // atualiza
    if (source_id) {
      // procura pelo tópico
      const topic = await Posts.findOne(source_id, ['author_id'])
      if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (1).')
      if (!topic) return res.fail('Tópico não encontrado.')

      // verifica se é autor
      const isOwner = topic.author_id.equals(author_id)
      if (!isOwner) return res.fail('Você precisa ser o autor deste tópico.')

      // atualiza
      await Posts.updateItem(source_id, data)
      if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (2).')

      // retorna
      res.json({})
    }
    // cadastra
    else {
      await Posts.add(Object.assign(data, {author_id}))
      if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (2).')

      // adiciona participante
      await Supporters.updateList({
        post_id: data.parent_id,
        author_id: author_id
      })
      if (Supporters.hasError) return res.fail('Ocorreu um erro no sistema (3).')

      // notifica nova fonte de boicote
      Mail.send({
        Subject: 'Nova fonte foi cadastrada - Boicote!',
        Message: `parent_id: ${data.parent_id}\r\ntitle: ${data.title}\r\text: ${data.text}\r\text: ${data.source}`
      })

      // retorna
      res.json({})
    }
  },

  async remove (req, res) {
    const source_id = purifix.objectId(req.body.source_id)
    const author_id = req.user._id

    // valida
    if (!source_id) return res.fail('Informe o tópico.')

    // procura pelo tópico
    const topic = await Posts.findOne(source_id, ['author_id'])
    if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (1).')
    if (!topic) return res.fail('Tópico não encontrado.')

    // verifica se é autor
    const isOwner = topic.author_id.equals(author_id)
    if (!isOwner) return res.fail('Você precisa ser o autor deste tópico.')

    // remove
    await Posts.removeOne(source_id)
    if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (2).')

    // retorna
    res.json({})
  },

}
