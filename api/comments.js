const purifix = require('purifix')
const assert = require('assertjs')
const Comments = require('../app/Comments.js')
const Posts = require('../app/Posts.js')
const Users = require('../app/Users.js')
const Mail = require('../app/Mail.js')
const Supporters = require('../app/Supporters.js')
const ObjectID = require('mongodb').ObjectID

module.exports = {

  async get (req, res) {
    const post_id = purifix.objectId(req.params.post_id)

    // valida
    if (!post_id) return res.fail('Comentário não encontrado.')

    const comments = await Comments.fromPost(post_id)
    if (Comments.hasError) return res.fail(Comments.hasError)

    res.json(comments)
  },

  async save (req, res) {
    const author_id = req.user._id
    const data = {
      post_id: purifix.objectId(req.body.post_id),
      text: purifix.string(req.body.text, 'trim'),
    }

    // valida
    if (!data.text) return res.fail('Escreva seu comentário.', 'text')

    // verifica se o tópico existe
    const exists = await Posts.findOne(data.post_id, ['_id'])
    if (Posts.hasError) return res.fail(Posts.hasError)
    if (!exists) return res.fail('Falhou, pois este tópico não existe mais.')

    // adiciona comentário
    Object.assign(data, { author_id, _id: new ObjectID() })
    await Comments.add(data)
    if (Comments.hasError) return res.fail(Comments.hasError)

    // adiciona participante
    const op = await Supporters.updateList({
      post_id: data.post_id,
      author_id: author_id
    })
    if (Supporters.hasError) return res.fail(Supporters.hasError)

    // notifica novo usuário cadastrado
    Mail.send({
      Subject: 'Novo comentário adicionado - Boicote!',
      Message: `${data.name} <${data.email}>`
    })

    // retorna
    res.json({comment: data})
  },

  async remove (req, res) {
    const comment_id = purifix.objectId(req.body.comment_id)
    const author_id = req.user._id

    // valida
    if (!comment_id) return res.fail('Informe o comentário.')

    // procura pelo comentário
    const comment = await Comments.findOne(comment_id, ['author_id'])
    if (Comments.hasError) return res.fail(Comments.hasError)
    if (!comment) return res.fail('Tópico não encontrado.')

    // verifica se é autor
    const isOwner = comment.author_id.equals(author_id)
    if (!isOwner) return res.fail('Você precisa ser o autor deste comentário.')

    // remove
    await Comments.delete(comment_id)
    if (Comments.hasError) return res.fail(Comments.hasError)

    // retorna
    res.json({})
  }

}
