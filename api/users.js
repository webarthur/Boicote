const md5 = require('md5')
const purifix = require('purifix')
const assert = require('assertjs')
const bcrypt = require('bcrypt')
const config = require('../config/settings.json')
const ObjectID = require('mongodb').ObjectID
const Users = require('../app/Users.js')
const Mail = require('../app/Mail.js')

module.exports = {

  credentials (req, res) {
    res.json(req.user || {})
  },

  async add (req, res) {
    // limpeza
    var data = {
      name: purifix.string(req.body.name, 'trim'),
      nick: md5(new Date().getTime()).substring(0, 5),
      avatar: 'gravatar://' + md5(purifix.string(req.body.email, 'trim')),
      email: purifix.string(req.body.email, 'trim'),
      pass: purifix.string(req.body.pass),
    }

    // valida
    assert(data)
    .the('name').isNotEmpty().or('Informe seu nome.')
    .the('name').isMinLength(2).or('Seu nome está muito curto.')
    .the('name').isMaxLength(40).or('Nome muito grande, tente reduzí-lo.')
    .the('email').isNotEmpty().or('Informe seu email.')
    .the('email').isEmail().or('E-mail inválido')
    .the('pass').isNotEmpty().or('Informe sua senha.')
    .the('pass').isMinLength(6).or('Sua senha deve ter no mínimo 6 dígitos.')
    if (assert.hasError) return res.fail(assert.error, assert.inputName)

    // verifica se o usuário já existe
    const exists = await Users.findOne({email: data.email}, ['_id'])
    if (exists) return res.fail('Este email já foi cadastrado.')

    // verifica se o nick existe
    while (await Users.findOne({nick: data.nick}, ['_id'])) {
      data.nick = md5(new Date().getTime()).substring(0, 5)
    }

    // criptografa senha
    data.pass = bcrypt.hashSync(data.pass, config.server.salt)

    // adiciona gravatar
    // data.avatar = 'https://www.gravatar.com/avatar/' + md5(data.email.toLowerCase())

    await Users.insertOne(data)
    if (Users.hasError) return res.fail(assert.error, assert.inputName)

    // notifica novo usuário cadastrado
    Mail.send({
      Subject: 'Novo usuário cadastrado - Boicote!',
      Message: `${data.name} <${data.email}>`
    })

    res.json({})
  }

}
