const Posts = require('../app/Posts.js')
const Supporters = require('../app/Supporters.js')
const Users = require('../app/Users.js')
const Mail = require('../app/Mail.js')
const slugify = require('slugify')
const purifix = require('purifix')
const assert = require('assertjs')
const isError = x => x instanceof Error

module.exports = {

  async getLasts (req, res) {
    const campos = ['title', 'slug', 'created_at', 'updated_at', 'num_reasons', 'num_users', 'num_votes_up', 'num_votes_down', 'tags']

    const topics = await Posts.find({parent_id: null}, campos, {limit: 20})
    if (Posts.hasError) return res.fail('Ocorreu um erro no sistema.')

    res.json(topics)
  },

  async get (req, res) {
    const slug = slugify(purifix.string(req.params.slug)) // purifix
    const author_id = req.user._id

    // valida
    if (!slug) return res.fail('Página não encontrada.')

    const posts = await Posts.aggregate([
      {$match: { slug: slug }},
      {$lookup: {
        'from': 'users',
        'localField': 'author_id',
        'foreignField': '_id',
        'as': 'author'
      }},
      {$unwind: '$author'},
      {$lookup: {
        'from': 'posts',
        'localField': '_id',
        'foreignField': 'parent_id',
        'as': 'reasons'
      }},
      {$project: {
        title: 1,
        text: 1,
        info: 1,
        tags: 1,
        sources: 1,
        reasons: {$filter: {
          input: '$reasons',
          as: 'item',
          cond: {$eq: [ '$$item.type', 'reasons' ]}
        }},
        // 'reasons.title': 1,
        'author.avatar': 1,
        'author.nick': 1,
        'author.name': 1,
        created_at: 1
      }}
    ])

    if (Posts.hasError) return res.fail(Posts.hasError)
    if (!posts[0]) return res.fail('Página não encontrada.')

    // verifica participação no tópico
    if (author_id) {
      const supporters = await Supporters.fromPost(posts[0]._id, author_id)
      if (Supporters.hasError) return res.fail(Supporters.hasError)
      posts[0].participates = Boolean(supporters && supporters[0] && supporters[0].active)
    }

    // retorna
    res.json(posts[0])
  },

  async join (req, res) {
    const author_id = req.user._id
    const data = {
      post_id: purifix.objectId(req.body.post_id),
      leave: purifix.boolean(req.body.leave, false)
    }

    // valida
    assert(data).the('post_id').isNotEmpty().or('Especifique o ID do tópico.')
    if (assert.hasError) return res.fail(assert.error, assert.inputName)

    // remove participante
    if (data.leave) {
      await Supporters.updateItem(data.post_id, author_id, {active: false})
      if (Supporters.hasError) return res.fail(Supporters.hasError)
    }
    // adiciona participante
    else {
      await Supporters.updateList({ post_id: data.post_id, author_id, active: true })
      if (Supporters.hasError) return res.fail(Supporters.hasError)
    }

    // retorna
    res.json({})
  },

  async save (req, res) {
    const post_id = purifix.objectId(req.body._id)
    const author_id = req.user._id
    const data = {
      title: purifix.string(req.body.title, 'trim'),
      slug: slugify(purifix.string(req.body.title).toLowerCase().trim()),
      text: purifix.string(req.body.text, 'trim'),
      info: purifix.string(req.body.info, 'trim'),
      tags: purifix.string(req.body.tags, 'trim')
    }

    // valida
    assert(data)
    .the('title').isNotEmpty().or('Informe o título.')
    .the('title').isMinLength(40).or('Defina melhor o tema, o título está muito curto.')
    .the('text').isNotEmpty().or('Descreva o tema a ser debatido.')
    .the('info').isNotEmpty().or('Descreva como deve ser realizado o boicote.')
    .the('text').isMinLength(80).or('Descreva melhor o contexto do boicote, o texto está muito curto.')
    .the('info').isMinLength(80).or('Descreva melhor como boicotar, o texto está muito curto.')
    .the('tags').isNotEmpty().or('Informe a quais tópicos está relacionado.')
    if (assert.hasError) return res.fail(assert.error, assert.inputName)

    // atualiza
    if (post_id) {

      // procura pelo tópico
      const topic = await Posts.findOne(post_id, ['author_id'])
      if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (1).')
      if (!topic) return res.fail('Tópico não encontrado.')

      // verifica se é author
      const isOwner = (topic.author_id.equals(author_id))
      if (!isOwner) return res.fail('Você precisa ser o author deste tópico.')

      // atualiza
      await Posts.updateItem(post_id, {
        title: data.title,
        slug: slugify(data.title).toLowerCase(),
        text: data.text,
        info: data.info,
        tags: data.tags
      })
      if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (2).')

      // adiciona participante
      await Supporters.updateList({ post_id, author_id })
      if (Supporters.hasError) return res.fail('Ocorreu um erro no sistema (3).')

      // retorna
      res.json({url: '/tema/' + data.slug})
    }
    // cadastra
    else {
      await Posts.add(Object.assign({}, data, {author_id}))
      if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (4).')

      // notifica novo boicote
      Mail.send({
        Subject: 'Novo boicote foi cadastrado - Boicote!',
        Message: data.slug
      })

      // retorna
      res.json({url: '/tema/' + data.slug})
    }
  },

  async remove (req, res) {
    const author_id = req.user._id
    const post_id = purifix.objectId(req.body.post_id)

    // valida
    if (!post_id) return res.fail('Informe o tópico.')

    // procura pelo tópico
    const topic = await Posts.findOne(post_id, ['author_id'])
    if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (1).')
    if (!topic) return res.fail('Tópico não encontrado.')

    // verifica se é author
    const isOwner = topic.author_id.equals(author_id)
    if (!isOwner) return res.fail('Você precisa ser o author deste tópico.')

    // remove
    await Posts.remove(post_id)
    if (Posts.hasError) return res.fail('Ocorreu um erro no sistema (2).')

    // retorna
    res.json({})
  }

}
