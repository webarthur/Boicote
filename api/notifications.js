const Notifications = require('../app/Notifications.js')

module.exports = {

  async get (req, res) {
    // procura pelo tópico
    const notifications = await Notifications.fromAutor(req.user._id)
    if (Notifications.hasError) return res.fail('Ocorreu um erro no sistema (1).')

    // retorna
    res.json({ notifications: notifications || [] })
  }

}
