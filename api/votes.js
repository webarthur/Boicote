const Votes = require('../app/Votes.js')
const Users = require('../app/Users.js')
const purifix = require('purifix')
const assert = require('assertjs')

module.exports = {

  async save (req, res) {
    const author_id = req.user._id
    const data = {
      post_id: purifix.objectId(req.body.post_id),
      vote: purifix.float(req.body.vote, -2)
    }

    // valida
    assert(data)
    .the('post_id').isNotEmpty().or('Especifique o ID do tópico.')
    .the('vote').isIn([1, 0, -1]).or('Voto não identificado.')
    if (assert.hasError) return res.fail(assert.error, assert.inputName)

    await Votes.save(data.post_id, author_id, data.vote)
    if (Votes.hasError) return res.fail(Votes.hasError)

    // retorna
    res.json({})
  }

}
