// const csrf = require('csurf')
const helmet = require('helmet')
const config = require('./config/index.js')
const express = require('express')
const expressFail = require('express-fail')
const session = require('express-session')
const passport = require('passport')
const bodyParser = require('body-parser')
const TwitterStrategy = require('passport-twitter').Strategy
const FacebookStrategy = require('passport-twitter').Strategy
const MongORM = require('mongorm')
const port = process.env.NODE_ENV === 'DEV' ? 8080 : config.server.port
const app = express()
const controller = (name) => require('./api/' + name + '.js')
const staticPage = file => (req, res) => res.sendFile(require('path').resolve(__dirname) + '/public/' + file)
const isAuthenticated = config.passport.local.isAuthenticated

// inicia a aplicação apenas se o banco de dados estiver ok
config.database(config, app).then(db => {
  MongORM.setDB(db)

  // Mailing configuration
  const Mail = require('./app/Mail.js')
  Mail.defaults = config.server.mail
  Mail.devMailing = config.server.devMailing
  Mail.save = config.server.saveMailing

  // const Posts = require('./app/Posts.js')
  // Posts.insertOne({test: 1})

  // segurança
  app.use(helmet())
  app.disable('x-powered-by')

  // configura sessões
  app.use(session(config.server.session))

  // configura autenticação
  app.use(passport.initialize())
  app.use(passport.session(config.server.session))
  app.use((req, res, next) => {req.user = req.session.user || {}; next()})

  // serve arquivos estáticos
  app.use(express.static('public', {
    index: 'index.html',
    extensions: ['html']
  }))

  // interpreta payload
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))

  // tratamento de erros
  app.use(expressFail({csrf: false}))

  // STATIC
  app.get('/tema/:slug', staticPage('topico.html'))
  app.get('/tema/:slug/editar', staticPage('topico-form.html'))

  // AUTH - LOCAL
  app.get('/auth/logout', config.passport.local.logout)
  app.post('/auth/login', config.passport.local.authenticate)

  // AUTH - FACEBOOK
  app.get('/auth/facebook', config.passport.facebook.authenticate)
  app.get('/auth/facebook/callback', config.passport.facebook.callback, config.passport.facebook.ok)

  // AUTH
  app.get('/api/user', controller('users').credentials)
  app.post('/api/user', controller('users').add)

  // TÓPICOS
  app.get('/api/topico/:slug', controller('posts').get)
  app.get('/api/posts', controller('posts').getLasts)
  app.post('/api/posts', isAuthenticated, controller('posts').save)
  app.post('/api/posts/join', isAuthenticated, controller('posts').join)
  app.delete('/api/posts', isAuthenticated, controller('posts').remove)

  // SUBTÓPICOS/FONTES
  app.get('/api/sources/:source_id', controller('sources').get)
  app.post('/api/sources', isAuthenticated, controller('sources').save)
  app.post('/api/sources/analyse', isAuthenticated, controller('sources').analyse)
  app.delete('/api/sources', isAuthenticated, controller('sources').remove)

  // COMENTÁRIOS
  app.get('/api/comments/:post_id', controller('comments').get)
  app.post('/api/comments', isAuthenticated, controller('comments').save)
  app.delete('/api/comments', isAuthenticated, controller('comments').remove)

  // VOTOS
  app.post('/api/votes', isAuthenticated, controller('votes').save)

  // NOTIFICAÇÕES
  app.get('/api/notifications', isAuthenticated, controller('notifications').get)

  // TESTES
  app.get('/api/ok', (req, res) => res.json({ok: 1}))

  app.listen(port)

  console.log('Servidor iniciado na porta ' + port)
})
