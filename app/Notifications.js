const ObjectID = require('mongodb').ObjectID
const isObjectID = _id => _id instanceof ObjectID
const isString = s => typeof s === 'string'
const isNumber = s => typeof s === 'number'

const MongORM = require('mongorm')

const Notifications = module.exports = MongORM('notifications', {

  errors: {
    no_type: 'Parâmetro type não definido em Notifications.add()',
    no_author_id: 'Parâmetro author não definido em Notifications.add()',
    no_trigger_id: 'Parâmetro trigger_id não definido em Notifications.add()',
    no_post_id: 'Parâmetro post_id não definido em Notifications.updateItem()',
    invalid: 'Parâmetro tipo é inválido Notifications.add()'
  },

  async add (data) {
    var { author_id, post_id, trigger_id, type, contador } = data

    // check
    if (!data.author_id) return this.fail(this.errors.no_author_id)
    if (!data.trigger_id) return this.fail(this.errors.no_trigger_id)
    if (!data.type) return this.fail(this.errors.no_type)
    if (!isNumber(data.type)) return this.fail(this.errors.type)

    // ajusts
    if (!isObjectID(data.post_id)) data.post_id = new ObjectID(data.post_id)
    if (!isObjectID(data.author_id)) data.author_id = new ObjectID(data.author_id)
    data.created_at = new Date()

    return await Notifications.insert(data)
  },

  async fromAuthor (author_id, query = {}, fields = {}) {
    // check
    if (!author_id) return this.fail(this.errors.no_author_id)

    // ajusts
    if (!isObjectID(author_id)) author_id = new ObjectID(author_id)
    query.author_id = author_id

    return await Notifications.find(query, fields)
  }

})
