const ObjectID = require('mongodb').ObjectID
const MongORM = require('mongorm')
const Posts = require('../app/Posts.js')

const Supporters = module.exports = MongORM('supporters', {

  usersCollection: 'users',

  errors: {
    no_title: 'Parâmetro title não definido em Supporters.add()',
    no_author_id: 'Parâmetro author não definido em Supporters.add()',
    no_text: 'Parâmetro text não definido em Supporters.add()',
    no_post_id: 'Parâmetro post_id não definido em Supporters.add()',
  },

  // adiciona se não está na lista
  async updateList (data) {
    // check
    if (!data.post_id) return this.fail(this.errors.no_post_id)
    if (!data.author_id) return this.fail(this.errors.no_author_id)

    // ajusts
    if (!MongORM.isObjectID(data.post_id)) data.post_id = new ObjectID(data.post_id)
    if (!MongORM.isObjectID(data.author_id)) data.author_id = new ObjectID(data.author_id)
    data.created_at = new Date()

    // verifica se já está apoiando
    const participates = await Supporters.findOne({
      post_id: data.post_id,
      author_id: data.author_id
    }, ['_id'])
    if (Supporters.hasError) return false

    if (participates) {
      return Supporters.updateItem(data.post_id, data.author_id, data)
    }
    else {
      return Supporters.insertOne(data)
    }
  },

  async updateItem (post_id, author_id, data) {
    // check
    if (!post_id) return this.fail(this.errors.no_post_id)
    if (!author_id) return this.fail(this.errors.no_author_id)

    // ajusts
    if (!MongORM.isObjectID(post_id)) post_id = new ObjectID(post_id)
    if (!MongORM.isObjectID(author_id)) author_id = new ObjectID(author_id)
    data.updated_at = new Date()

    return await Supporters.set({post_id, author_id}, data)
  },

  async delete (post_id, author_id) {
    // check
    if (!post_id) return this.fail(this.errors.no_post_id)
    if (!author_id) return this.fail(this.errors.no_author_id)

    // ajusts
    if (!MongORM.isObjectID(post_id)) post_id = new ObjectID(post_id)
    if (!MongORM.isObjectID(author_id)) author_id = new ObjectID(author_id)
    dados.updated_at = new Date()

    return await Supporters.remove({post_id, author_id})
  },

  fromPost (post_id, user_id) {
    // check
    if (!post_id) return this.fail('Parâmetro post_id não definido em Supporters.fromPost()')
    if (!user_id) return this.fail('Parâmetro user_id não definido em Supporters.fromPost()')

    // ajusts
    if (!MongORM.isObjectID(post_id)) post_id = new ObjectID(post_id)
    if (!MongORM.isObjectID(user_id)) user_id = new ObjectID(user_id)

    var match = { post_id: post_id }
    if (user_id) match.author_id = user_id
    return Supporters.aggregate([
      {$match: match},
      {$lookup: {
        'from': Supporters.usersCollection,
        'localField': 'author_id',
        'foreignField': '_id',
        'as': 'author'
      }},
      {$unwind: '$author'}
    ])
  }

})
