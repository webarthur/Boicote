// baseado em https://docs.mongoComments.db.com/ecosystem/use-cases/storing-comments/

const ObjectID = require('mongodb').ObjectID
const isObjectID = _id => _id instanceof ObjectID
const isString = s => typeof s === 'string'
const MongORM = require('mongorm')

const Comments = module.exports = MongORM('comments', {

  usersCollection: 'users',

  errors: {
    no_title: 'Parâmetro title não definido em Comments.add()',
    no_author_id: 'Parâmetro author não definido em Comments.add()',
    no_text: 'Parâmetro text não definido em Comments.add()',
    no_post_id: 'Parâmetro post_id não definido em Comments.add()',
    no_comments_id: 'Parâmetro comments_id não definido em Comments.updateItem()'
  },

  async add (data) {
    // check
    if (!data.post_id) return this.fail(this.errors.no_post_id)
    if (!data.author_id) return this.fail(this.errors.no_author_id)
    if (!data.text) return this.fail(this.errors.text)

    // ajusts
    if (!isObjectID(data.post_id)) data.post_id = new ObjectID(data.post_id)
    if (!isObjectID(data.author_id)) data.author_id = new ObjectID(data.author_id)
    if (data.parent_id && !isObjectID(data.parent_id)) data.parent_id = new ObjectID(data.parent_id)
    data.created_at = new Date()

    // insert comment
    return await Comments.insertOne(data)
  },

  async updateItem (comment_id, data) {
    // check
    if (!comment_id) return this.fail(this.erros.no_comments_id)

    // ajusts
    if (!(isObjectID(comment_id))) comment_id = new ObjectID(comment_id)
    if (data.parent_id) {
      data.parent_id = !isObjectID(data.parent_id) ? new ObjectID(data.parent_id) : data.parent_id
    }
    data.updated_at = new Date()

    // update
    return await Comments.setOne(comment_id, data)
  },

  async delete (comment_id) {
    // check
    if (!comment_id) return this.fail('Parâmetro comment_id não definido em Comments.add()')

    // ajusts
    if (!(isObjectID(comment_id))) comment_id = new ObjectID(comment_id)

    return await Comments.removeOne(
      {$or: [
        {_id: comment_id}, // remove comment
        {parent_id: comment_id} // remove comment children
      ]}
    )
  },

  async fromPost (post_id) {
    // check
    if (!post_id) return this.fail(this.errors.no_post_id)

    // ajusts
    if (!isObjectID(post_id)) post_id = new ObjectID(post_id)

    return await Comments.aggregate([
      {$match: {post_id: post_id}},
      {$lookup: {
        'from': Comments.usersCollection,
        'localField': 'author_id',
        'foreignField': '_id',
        'as': 'author'
      }},
      {$unwind: '$author'},
      {$project: {
        text: 1,
        created_at: 1,
        updated_at: 1,
        author: {
          _id: 1,
          name: 1,
          nick: 1,
          avatar: 1
        }
      }}
    ])
  }

})
