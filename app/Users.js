const ObjectID = require('mongodb').ObjectID

const md5 = require('md5')
const bcrypt = require('bcrypt')
const MongORM = require('mongorm')

const Users = module.exports = MongORM('users', {

  verifyPassword (pass, hash) {
    return bcrypt.compareSync(pass, hash)
  },

  gravatar (email) {
    if (!email) return ''
    return 'https://www.gravatar.com/avatar/' + md5(email.toLowerCase().trim())
  },

  fbavatar (id) {
    return 'https://graph.facebook.com/' + id + '/picture'
  }

})
