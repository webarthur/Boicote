const ObjectID = require('mongodb').ObjectID
const slugify = require('slugify')
const MongORM = require('mongorm')
const isObjectID = MongORM.isObjectID

const Posts = module.exports = MongORM('posts', {

  errors: {
    no_title: 'Parâmetro title não definido em Posts.add()',
    no_author_id: 'Parâmetro author não definido em Posts.add()',
    no_text: 'Parâmetro text não definido em Posts.add()',
    no_post_id: 'Parâmetro post_id não definido em Posts.updateItem()'
  },

  async add (data) {
    // check
    if (!data.title) return this.fail(this.errors.no_title)
    if (!data.author_id) return this.fail(this.errors.no_author_id)
    if (!data.text) return this.fail(this.errors.text)

    // ajusts
    const slug = data.slug ? slugify(data.slug.toLowerCase()) : slugify(data.title.toLowerCase())
    data.slug = slug
    if (!isObjectID(data.author_id)) data.author_id = new ObjectID(data.author_id)
    data.parent_id = data.parent_id ? new ObjectID(data.parent_id) : null
    data.created_at = new Date()

    // check if the slug exists
    var slugCounter = 1
    while (await Posts.findOne({slug: data.slug}, ['_id'])) {
      slugCounter += 1
      data.slug = slug + '-' + slugCounter
    }

    // check for errors
    if (Posts.hasError) return false

    // insert the post
    return await Posts.insert(data)
  },

  async updateItem (post_id, data) {
    // check
    if (!post_id) return this.fail(this.erros.no_post_id)

    // ajusts
    const slug = data.slug
    if (!isObjectID(post_id)) post_id = new ObjectID(post_id)
    if (data.parent_id) {
      data.parent_id = !isObjectID(data.parent_id) ? new ObjectID(data.parent_id) : data.parent_id
    }
    data.updated_at = new Date()

    // check if the slug exists
    var slugCounter = 1
    while (await Posts.findOne({ slug: data.slug, _id: {$ne: post_id} }, ['_id'])) {
      slugCounter += 1
      data.slug = slug + '-' + slugCounter
    }

    // check for errors
    if (Posts.hasError) return false

    // update
    return await Posts.set({_id: post_id}, data)
  },

  async delete (post_id) {
    // check
    if (!post_id) return this.fail(this.errors.no_post_id)

    // ajusts
    if (!isObjectID(post_id)) post_id = new ObjectID(post_id)

    return await Posts.removeOne(
      {$or: [
        {_id: post_id}, // remove post
        {parent_id: post_id} // remove comments
      ]}
    )
  }

})
