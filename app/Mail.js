const request = require('request')
const MongORM = require('mongorm')

const Mail = module.exports = MongORM('emails', {

  save: false,
  devMailing: false,
  defaults: {
    // url: 'https://example.org/mailing',
    method: 'POST',
    headers: {
      'Content-Type':     'application/x-www-form-urlencoded'
    },
    form: {
      // 'To': 'client@example.org',
      // 'From': 'admin@example.org',
      // 'Message': '...'
    }
  },

  send (form, headers) {
    const save = this.save
    const devMailing = this.devMailing
    var options = this.defaults

    // configure querystring
    options.form = Object.assign({}, this.defaults.form, form)

    // configure headers
    if (headers) options.headers = Object.assign({}, this.defaults.headers, headers)

    // start the request
    return new Promise((resolve, reject) => {
      if (save) {
        Mail.insertOne(Object.assign({}, options.form, {headers: options.headers}))
      }

      if (!devMailing) return Promise.resolve(true)

      request(options, function (error, response, body) {
        if (error) return reject(error)
        resolve(response, body)
      })
    })

  }

})
