const ObjectID = require('mongodb').ObjectID
const isObjectID = _id => _id instanceof ObjectID
const isString = s => typeof s === 'string'

const MongORM = require('mongorm')

const Votes = module.exports = MongORM('votes', {

  usersCollection: 'users',

  errors: {
    no_title: 'Parâmetro title não definido em Votes.add()',
    no_author_id: 'Parâmetro author não definido em Votes.add()',
    no_text: 'Parâmetro text não definido em Votes.add()',
    no_post_id: 'Parâmetro post_id não definido em Votes.updateItem()'
  },

  async save (post_id, author_id, vote) {
    // check
    if (!post_id) return this.fail('Parâmetro post_id não definido em Votes.item()')
    if (!author_id) return this.fail('Parâmetro author_id não definido em Votes.item()')
    if ([1, -1, 0].indexOf(vote) === -1) return this.fail('Parâmetro voto é inválido')

    // ajusts
    if (!isObjectID(post_id)) post_id = new ObjectID(post_id)
    if (!isObjectID(author_id)) author_id = new ObjectID(author_id)
    if (vote === 1) vote = true
    if (vote === -1) vote = false

    // find for
    const doc = await Votes.findOne({ post_id, author_id }, ['vote'])
    if (Votes.hasError) return false

    const voted = doc && (typeof doc.vote === 'boolean')
    const noVote = !voted

    // add vote
    if (noVote && vote !== 0) {
      return await Votes.insertOne({vote, post_id, author_id, created_at: new Date()})
    }
    // change vote
    else if (voted && vote !== 0 && doc.vote !== vote) {
      return await Votes.set({post_id, author_id}, {vote, updated_at: new Date()})
    }
    // remove vote
    else if (vote === 0 && voted) {
      return await Votes.removeOne({post_id, author_id})
    }
  },

  async fromPost (post_id) {
    // check
    if (!post_id) return this.fail('Parâmetro post_id não definido em Votes.fromPost()')

    // ajusts
    if (!isObjectID(post_id)) post_id = new ObjectID(post_id)

    return await Votes.find({post_id})
  },

  async fromPostWithAutor (post_id) {
    // check
    if (!post_id) return this.fail('Parâmetro post_id não definido em Votes.fromPostWithAutor()')

    // ajusts
    if (!isObjectID(post_id)) post_id = new ObjectID(post_id)

    // retorn a promise
    return await Votes.aggregate([
      {$match: {post_id: post_id}},
      {$lookup: {
        'from': Votes.usersCollection,
        'localField': 'author_id',
        'foreignField': '_id',
        'as': 'author'
      }},
      {$unwind: '$author'},
      {$project: {
        text: 1,
        created_at: 1,
        updated_at: 1,
        author: {
          _id: 1,
          name: 1,
          nick: 1,
          avatar: 1
        }
      }}
    ])
  }

})
