<!DOCTYPE html>
<html>
<head>
  <title>[[APPNAME]] - [[APPDESC]]</title>
  [[METAS]]
</head>
<body>
[[HEADER]]

<div cols="9" style="padding-right:30px">

  <main layout="margin-20 left" hidden>

    <h1 id="formTitle">Adicionar um tema para boicote:</h1>

    <form id="formPost" action="index.html" method="post" onsubmit="Post.save(this, event)">
      <p>
        <input type="text" name="title" layout="fluid" placeholder="Qual o tema do boicote? Seja bem específico.">
      </p>
      <p>
        <label text="small bold" for="text">Explique o contexto:</label>
        <textarea name="text" layout="fluid" rows="4"></textarea>
      </p>
      <p>
        <label text="small bold" for="info">Como boicotar?</label>
        <textarea name="info" layout="fluid" rows="4"></textarea>
      </p>
      <p>
        <label text="small bold" for="tags">Palavras-chave:</label>
        <input type="text" name="tags" layout="fluid" placeholder="Ex.: política, mídia, etc">
      </p>
      <p layout="fix">
        <input type="hidden" name="_id">

        <button class="hideIfEdit" layout="left" type="submit">Iniciar boicote!</button>
        <button class="showIfEdit" layout="left" type="submit" hidden>Salvar</button>

        <button type="cancelar" layout="right" onclick="event.preventDefault(); Page.goBack()">Cancelar</button>
      </p>
    </form>

  </main>

</div>

<aside cols="3"></aside>

<script>
function __init () {
  // edição
  if (/\/editar$/.test(location.href)) {
    $get('formTitle').innerHTML = 'Editar tema:'
    Post.edit()
  }
  // cadastro
  else {
    $('main').show()
  }

  // foco no campo
  $find('#formPost input[name=title]').focus()
}
</script>

[[FOOTER]]
