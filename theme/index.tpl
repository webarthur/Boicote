<!DOCTYPE html>
<html>
<head>
  <title>[[APPNAME]] – [[APPDESC]]</title>
  [[METAS]]
</head>
<body>
[[HEADER]]

<div cols="8">

  <div layout="margin-20 left">

    <h1>Temas debatidos</h1>

    <ul id="postList" layout="flat" data-template hidden>
      {{#posts}}
      <li onclick="Page.go('/tema/{{slug}}')" text="1.1em line-1.5" layout="padding-10 fix" style="border-bottom:1px solid rgba(0, 0, 0, .1)" onhover="pointer">
        <h3 text="1em bold" layout="no-margin">{{title}}</h3>
        <!-- <p text=".8em blue" layout="no-margin">tecmundo.com.br</p> -->
        <!-- <p text=".8em gray bold" layout="no-margin">
          <i class="material-icons" style="font-size:1.9em">&#xE316;</i>{{num_votes_up}}
          <i class="material-icons" style="font-size:1.9em">&#xE313;</i>{{num_votes_down}}
          <i class="material-icons" style="font-size:1.3em">&#xE0CA;</i>{{num_comments}}
          <i class="material-icons" style="font-size:1.3em">&#xE8A6;</i>{{num_users}}
        </p> -->
      </li>
      {{/posts}}
    </ul>

  </div>

</div>

<aside id="sidebar" class="loginArea" cols="4" hidden>
  <section layout="gray-10">
    <h2 text="1em upper bold black-80" layout="top-10">Login</h2>

    <div text="center">
      <button onclick="Page.go('/auth/facebook')">
        <i class="fa fa-facebook"></i>
        Conectar com Facebook
      </button>
    </div>

    <div class="line">
      <span layout="gray-10">ou</span>
    </div>

    <form id="formLogin" layout="fix" onsubmit="Auth.login(this, event); return false">
      <p>
        <input type="text" name="email" layout="fluid" placeholder="E-mail">
      </p>
      <p>
        <input type="password" name="pass" layout="fluid" placeholder="Senha">
      </p>
      <p>
        <button type="submit">Login</button>
      </p>
    </form>

    <hr layout="top-20">

    <h2 text="1em upper bold black-80" layout="top-10">Cadastro</h2>
    <form id="formCadastro" layout="fix" action="" onsubmit="Auth.signup(this, event)">
      <p>
        <input type="text" name="name" layout="fluid" placeholder="Nome">
      </p>
      <p>
        <input type="text" name="email" layout="fluid" placeholder="E-mail">
      </p>
      <p>
        <input type="password" name="pass" layout="fluid" placeholder="Senha">
      </p>
      <p>
        <button type="submit">Cadastrar</button>
      </p>
    </form>

  </section>
</aside>



<script>
function __init () {
  $.get('/api/posts', function (posts) {
    Template.render('postList', { posts: posts })
  })
}
</script>

[[FOOTER]]
