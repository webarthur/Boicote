
<div id="modalSource" data-template hidden>
  <header layout="flex bottom-10 fix">
    <div layout="right-10" style="margin-top:-8px">
      <i class="material-icons vote-up {{voteUp}}" onhover="pointer" style="font-size:2.8em" onclick="Post.vote(1, this)">&#xE316;</i> <br>
      <i class="material-icons vote-down {{voteDown}}" onhover="pointer" style="font-size:2.8em" onclick="Post.vote(-1, this)">&#xE313;</i>
    </div>
    <div layout="auto grow">
      <h2 text="line-1" layout="no-margin no-bottom fix">
        {{{title}}}
      </h2>
      <p text="small" style="margin-top:-10px">
        <a href="{{source}}" target="_blank">{{source}}</a>
      </p>
    </div>
    <div>
      <i id="btnEditSource" class="material-icons">&#xE5D4;</i>
    </div>
  </header>

  {{{text}}}

  <div class="box" layout="flex row items-center bottom-20">
    <div text="1.4em gray-20" layout="grow">
      <!-- TOOLBARS -->
    </div>
    <div text="inline-block gray-50 small" layout="right-15">
      {{dataFormatada}}
    </div>
    <div>
      <img text="middle" src="{{author.avatar}}" alt="{{author.name}}" layout="circle" width="25">
      <b text="small">{{author.name}}</b>
    </div>
  </div>

  <p text=".8em gray" layout="flex row items-center no-margin" style="padding-bottom:15px">
    <span layout="grow">{{num_comments}} comentários &nbsp; </span>
  </p>

  <ul id="modalComments" data-template layout="flat">
    {{#comments}}
    <li id="comment_{{_id}}" text="line-1.5" layout="v-padding-10 fix" style="border-top:1px solid rgba(0, 0, 0, .1)">
      <div layout="left">
        <img src="{{avatarURL}}" alt="{{author.name}}" layout="circle" width="40">
      </div>
      <div layout="auto" style="padding-left:10px">
        <div layout="flex">
          <h3 text=".8em bold" layout="grow no-margin">
            {{author.name}} <span text="normal gray-30">@{{author.nick}} {{humanDate}}</span>
            {{btnRemoveComment}}
          </h3>
          <span class="btn-remove" onclick="Comment.removeIfConfim('{{_id}}')" layout="{{btnRemoveComment}}">&times;</span>
        </div>
        <div text=".8em" layout="no-margin">
          {{{comment}}}
        </div>
      </div>
    </li>
    {{/comments}}
  </ul>

  <div class="rBox" layout="flex items-center" style="margin:20px -32px -32px">
    <div layout="right-15">
      <img src="{{#user}}{{avatarURL}}{{/user}}" alt="{{user.name}}" layout="circle" width="40">
    </div>
    <textarea id="commentBox" rows="1" layout="grow" placeholder="Comente sobre o assunto" onkeyup="Comment.updateSendButton(this)" onchange="Comment.updateSendButton(this)"></textarea>
    <div id="commentSendButton" layout="left-15" class="disable" onclick="Comment.save('{{_id}}', 'modalComments')">
      <i class="material-icons">&#xE163;</i>
    </div>
  </div>

</div>
