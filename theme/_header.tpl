
<header>
  <div class="maxWidth" text="black-80" layout="flex row items-center justify-center h-padding-15" style="height:70px">
    <a href="/" class="main-title">
      <strong text="giant">
        <i class="fa fa-ban" text="red"></i>
        Boicote!
      </strong>
    </a>
    <!-- <menu text="right" layout="grow right-40" links="gray-60 hover-blue">
      <a href="#">APRENDA</a>
      <a href="#">SOBRE</a>
      <a href="#">APP</a>
    </menu> -->
    <div id="loginBox" layout="flex items-center grow" text="right" links="gray-60 hover-blue" data-template>
      <div layout="grow">
        <a layout="inline-block" href="/topico-form">Iniciar um boicote!</a>
      </div>
      &nbsp; &nbsp; &nbsp;
      <nav class="show-if-{{logged}}" onclick="this.classList.toggle('active')" hidden>
        <img data-src="{{avatarURL}}" alt="{{nome}}" width="50" layout="circle left">
        <menu>
          <!-- <a href="#">Perfil</a> -->
          <!-- <a href="#">Configurações</a> -->
          <a href="#" onclick="Auth.logout(event)">Sair</a>
        </menu>
      </nav>
      <div class="show-if-not-{{logged}}" layout="inline-block" hidden>
        <button type="button" onclick="Modal.open(Template.data.modalLogin, ['button', 'escape'], 400)">
          login
        </button>
        &nbsp;
        <button type="outline" onclick="Modal.open(Template.data.modalSignup, ['button', 'escape'], 400)">cadastro</button>
      </div>
    </div>
  </div>
</header>

<main>
  <div class="maxWidth">
