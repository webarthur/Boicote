<meta charset="utf-8" />
<link rel="stylesheet" href="/css/helpers.min.css">
<link rel="stylesheet" href="/css/marx-2.0.7.min.css">
<link rel="stylesheet" href="/css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/material-design-icons-3.0.1/iconfont/material-icons.css">
<link rel="stylesheet" href="/css/tingle-0.11.0.min.css">
<link rel="stylesheet" href="/app.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600|Open+Sans:400,600">
<!-- <link rel="manifest" href="/manifest.json"> -->
<!-- <link rel="icon" href="/img/boicote.png" /> -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
