
<div id="modalSignup" data-template hidden>
  <h2 class="hideIfEdit" layout="no-top bottom-20">Cadastro:</h2>

  <div text="center">
    <button onclick="Page.go('/auth/facebook')">
      <i class="fa fa-facebook"></i>
      Conectar com Facebook
    </button>
  </div>

  <div class="line">
    <span layout="white">ou</span>
  </div>

  <form id="formCadastro" layout="fix" action="" onsubmit="Auth.signup(this, event)">
    <p>
      <label text="small bold" for="name">Nome:</label>
      <input type="text" name="name" layout="fluid" placeholder="Informe seu nome">
    </p>
    <p>
      <label text="small bold" for="email">E-mail:</label>
      <input type="text" name="email" layout="fluid" placeholder="Informe seu email">
    </p>
    <p>
      <label text="small bold" for="pass">Senha:</label>
      <input type="password" name="pass" layout="fluid" placeholder="******">
    </p>
    <p>
      <button type="submit" layout="left">Cadastrar</button>
      <button type="cancelar" layout="right" onclick="Modal.close(event)">Cancelar</button>
    </p>
  </form>
</div>
