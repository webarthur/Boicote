
<div id="modalFormFonte" data-template hidden>
  <h2 class="hideIfEdit" layout="no-top bottom-20">Adicionar uma nova fonte:</h2>
  <h2 class="showIfEdit" layout="no-top bottom-20" hidden>Editar fonte:</h2>
  <form id="formSource" action="" onsubmit="Source.save(this, event)">
    <p>
      <label text="small bold" for="source">Link da fonte<span text="orange">*</span>:</label>
      <input type="text" name="source" layout="fluid" placeholder="Cole o link da fonte a ser discutida. Pode ser de um texto, áudio, imagem ou vídeo" onchange="Source.analyse(this.value)">
    </p>
    <p>
      <label text="small bold" for="title">Título da fonte<span text="orange">*</span>:</label>
      <input type="text" name="title" layout="fluid" placeholder="Título contido no link da fonte">
    </p>
    <p>
      <label text="small bold" for="text">Resumo<span text="orange">*</span>:</label>
      <textarea name="text" layout="fluid" rows="2" placeholder="Escreva uma descrição da fonte"></textarea>
    </p>
    <p>
      <!-- <input type="hidden" name="_id"> -->
      <input type="hidden" name="type">
      <button type="cancelar" layout="right" onclick="Modal.close(event)">Cancelar</button>
      <button class="hideIfEdit" type="submit">Adicionar</button>
      <button class="showIfEdit" type="submit" hidden>Atualizar</button>
    </p>
  </form>
</div>
