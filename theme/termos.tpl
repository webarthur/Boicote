<!DOCTYPE html>
<html>
<head>
  <title>[[APPNAME]] – [[APPDESC]]</title>
  [[METAS]]
</head>
<body>
[[HEADER]]

<div cols="12">
  <h1>Política de privacidade e Termos de uso</h1>
  <h2>Política de privacidade para <a href='http://boicote.labpirata.com'>Boicote!</a></h2>
  <p>Todas as suas informações pessoais recolhidas, serão usadas para o ajudar a tornar a sua visita no nosso site o mais produtiva e agradável possível.</p>
  <p>A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é importante para o Boicote!.</p>
  <p>Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes que usem o Boicote! serão tratadas em concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro de 1998 (Lei n.º 67/98).</p>
  <p>A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou telemóvel, morada, data de nascimento e/ou outros.</p>
  <p>O uso do Boicote! pressupõe a aceitação deste Acordo de privacidade. A equipa do Boicote! reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos que consulte a nossa política de privacidade com regularidade de forma a estar sempre atualizado.</p>
  <h2>Os Cookies</h2>
  <p>Utilizamos cookies para armazenar informação, tais como as suas preferências pessoas quando visita o nosso website.</p>
  <p>Em adição também utilizamos publicidade de terceiros no nosso website para suportar os custos de manutenção. Alguns destes publicitários, poderão utilizar tecnologias como os cookies e/ou web beacons quando publicitam no nosso website.</p>
  <p>Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou efetuando alterações nas ferramentas de programas Anti-Virus, como o Norton Internet Security. No entanto, isso poderá alterar a forma como interage com o nosso website, ou outros websites. Isso poderá afetar ou não permitir que faça logins em programas, sites ou fóruns da nossa e de outras redes.</p>
  <h2>Ligações a Sites de terceiros</h2>
  <p>O Boicote! possui ligações para outros sites, os quais, a nosso ver, podem conter informações / ferramentas úteis para os nossos visitantes. A nossa política de privacidade não é aplicada a sites de terceiros, pelo que, caso visite outro site a partir do nosso deverá ler a politica de privacidade do mesmo.</p>
  <p>Não nos responsabilizamos pela política de privacidade ou conteúdo presente nesses mesmos sites.</p>
  <h2>Alterações na política de privacidade</h2>
  <p>O Boicote! reserva-se o direito de, a qualquer momento, modificar, alterar, acrescentar ou remover partes desta política de privacidade, atribuindo-se o dever de assegurar o sigilo, a proteção e a segurança dos dados e informações pessoais ou privados dos usuários.</p>
</div>



<script>
function __init () {
  $.get('/api/posts', function (posts) {
    Template.render('postList', { posts: posts })
  })
}
</script>

[[FOOTER]]
