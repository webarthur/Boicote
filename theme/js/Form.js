var Form = {

  serialize: function (form) {
    if (!form || form.nodeName !== 'FORM') return {}

    var o = {}
    var len = form.elements.length

    for (var i = 0; i < len; i++) {
      var e = form.elements[i]

      if (e.name === '') continue

      if (e.nodeName === 'INPUT') {
        if ((e.type === 'radio' || e.type === 'checkbox') && e.checked) {
          o[e.name] = e.value
        }
        else if (e.type === 'file') {} // o[e.name] = e.value
        else o[e.name] = e.value
      }
      else if (e.nodeName === 'TEXTAREA') {
        o[e.name] = e.value
      }
      else if (e.nodeName === 'SELECT') {
        if (e.type === 'select-one') {
          o[e.name] = e.value
        }
        else {
          for (var j = 0; j < e.options.length; j++) {
            if (e.options[j].selected) {
              o[e.name] = e.options[j].value
            }
          }
        }
      }
      // else if (e.nodeName === 'BUTTON') {
      //   o[e.name] = e.value
      // }
    }
    return o
  },

  populate: function (form, o) {
    if (typeof form === 'string') form = document.querySelector(form)
    if (!form || form.nodeName !== 'FORM') return {}

    Object.keys(o).forEach(function (key) {
      var e = form.elements[key]
      if (typeof e !== 'undefined') {
        // checkbox
        if (!e.type && e.length > 0 && o[key].length > 0) {
          for (var i = 0; i < e.length; i++) {
            if (o[key].indexOf(e[i].value) > -1) {
              e[i].checked = true
            }
          }
        } else if (e.type !== 'file') {
          e.value = o[key]
        }
      }
    })
    return form
  }

}
