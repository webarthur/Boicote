var Auth = {

  login: function (form, e) {
    e && e.preventDefault()
    $.post('/auth/login', Form.serialize(form), function (res) {
      if (res.error) return Template.message(res.error, form.elements['email'])
      Page.reload()
      Modal.close(e)
    })
  },

  logout: function (e) {
    e && e.preventDefault()
    $.get('/auth/logout', function (res) {
      if (res.error) return Modal.alert(res.error)
      Page.reload()
    })
  },

  signup: function (form, e) {
    e && e.preventDefault()
    $.post('/api/user', Form.serialize(form), function (res) {
      if (res.error) return Template.message(res.error, res.alvo ? form.elements[res.alvo] : null)
      Page.reload()
    })
  },

  forgotPassword: function (form, e) {
  },

  redefinePassword: function () {
  },

  recovery: function () {
  }

}
