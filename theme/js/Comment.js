var Comment = {

  scope: {
    humanDate: function () {
      return formatDate(this.created_at, 'human')
    },
    comment: function () {
      return marked(escapeHTML(this.text))
    },
    date: function () {
      return formatDate(this.created_at)
    },
    btnRemoveComment: function () {
      return this.author._id !== User.data._id ? 'hidden' : ''
    }
  },

  load: function (post_id, target) {
    $.get('/api/comments/' + post_id, function (comments) {
      Comment.scope.comments = comments
      Template.render(target, Template.scope(Comment.scope))
    })
  },

  save: function (post_id, target) {
    var comment = $get('commentBox').value
    if (!comment.trim()) return false

    var data = {
      post_id: post_id,
      text: comment
    }

    $.post('/api/comments', data, function (res) {
      if (res.error) return Template.message(res.error)

      var $modalComments = $get('modalComments')

      // adiciona autor
      res.comment.author = User.data

      Comment.scope.comments = [res.comment]

      // append
      $('#' + target).append(Template.renderHTML(Template.data[target], Template.scope(Comment.scope)))

      // reset
      $get('commentBox').value = ''
      $get('commentSendButton').classList.add('disable')
    })
  },

  updateSendButton: function (input) {
    var fn = input.value.trim() === '' ? 'add' : 'remove'
    $get('commentSendButton').classList[fn]('disable')
  },

  removeIfConfim: function (id) {
    Modal.confirm('Remover este comentário?',
      '<div class="rBox modal-footer" text="right">'
      + '  <button onclick="Comment.remove(\'' + id + '\')">Remover</button> &nbsp;'
      + '  <button type="cancelar" onclick="Modal.closeConfirm()">Cancelar</button>'
      + '</div>'
    )
  },

  remove: function (id) {
    Modal.closeConfirm()
    $.delete('/api/comments', {comment_id: id}, function (res) {
      if (res.error) return Modal.alert(res.error)
      $('#comment_' + id).slideUp(function () {
        $(this).remove()
      })
    })
  }

}
