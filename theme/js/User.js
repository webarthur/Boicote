var User = {

  credentials: function () {
    $.get('/api/user', function (session) {
      if (session.erro) Modal.alert(session.erro)
      if (session && session._id) {
        User.data = session
        User.data.logged = 1

        // render
        Template.render('loginBox', Template.scope(session))
      }
      else {
        User.data = { avatar: '/img/guest.png' }
        User.data.logged = 0

        Template.render('loginBox', User.data)

        // $get('guestArea').removeAttribute('hidden')
        var $loginArea = $find('.loginArea')
        if ($loginArea) $loginArea.removeAttribute('hidden')
      }
    })
  }

}
