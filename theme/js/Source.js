var Source = {

  analyse: function (source) {
    $.post('/api/sources/analyse', { source: source }, function (res) {
      if (res.error) return Modal.alert(res.error)

      var form = document.querySelector('.tingle-modal form')

      if (res.dados.title) form.elements.title.value = res.dados.title
      if (res.dados.text) form.elements.text.value = res.dados.text
    })
  },

  modal: function (post_id, type) {
    Modal.open(Template.loading)
    $.get('/api/sources/' + post_id, function (source) {
      source.type = type
      Cache.source = Object.assign({}, source)

      source.date = function () {
        return formatDate(this.created_at)
      }

      source.humanDate = function () {
        return formatDate(this.created_at, 'human')
      }

      source.voteUp = function () {
        if (!this.votes) return ''
        return this.votes.filter(function (x) {
          return x.author_id === User.data._id && x.vote === true
        }).length ? 'selected' : ''
      }

      source.voteDown = function () {
        if (!this.votes) return ''
        return this.votes.filter(function (x) {
          return x.author_id === User.data._id && x.vote === false
        }).length ? 'selected' : ''
      }

      // ajustes
      source.text = escapeHTML(source.text)
      source.text = marked(source.text)
      source.title = escapeHTML(source.title)
      source.title = marked(source.title)

      // popula usuario
      source.user = User.data

      $('.tingle-modal-box__content').html(Template.renderHTML(Template.data.modalSource, Template.scope(source)))

      $('#btnEditSource').fu_popover({
        content:
        '<ul layout="flat" class="popover-menu">'
        + '<li onclick="Source.modalForm(\'edit\')">Editar</li>'
        + '<li>Copiar link</li>'
        + '<li>Convidar pessoas</li>'
        + '<li>Denunciar abuso</li>'
        + '<li class="divisor"></li>'
        + '<li onclick="Modal.confirm(\'Deseja remover esta fonte da discussão?\')">Remover fonte</li>'
        + '</ul>',
        placement: 'bottom',
        dismissable: true
      })

      // popula comentários
      Comment.load(post_id, 'modalComments')
    })
  },

  modalForm: function (type) {
    Modal._modal && Modal._modal.close()
    Modal.open(Template.data.modalFormFonte, ['button', 'escape'])

    if (type === 'edit') {
      Form.populate('#formSource', Cache.source)
      $('.hideIfEdit').hide()
      $('.showIfEdit').show()
      $('#formSource input[name=source]').attr('disabled', 'disabled')
      $('#formSource input[name=title]').focus()
    }
    else {
      Cache.source = {}
      $('#modalFormFonteAvatar').attr('src', User.data.avatar)
      $('#formSource input[name=source]').focus()
      $('#formSource input[name=type]').val(type)
    }

    $('#btnEditSource').fu_popover('destroy')
  },

  save: function (form, e) {
    e.preventDefault()

    var data = Form.serialize(form)
    data.source_id = Cache.source._id
    data.parent_id = Cache.post_id

    if (!data.source.trim()) return Template.message('Você precisa indicar uma fonte.', form.elements.source)
    if (!data.title.trim()) return Template.message('A fonte precisa ter um título.', form.elements.title)
    if (!data.text.trim()) return Template.message('A fonte precisa ter um resumo.', form.elements.text)
    if (data.text.trim().length < 100) return Template.message('Defina melhor o resumo da fonte, o texto está muito curto.', form.elements.text)

    // A fuligem produzida num ambiente urbano pode contaminar o alimento produzido por hortas urbanas
    $.post('/api/sources', data, function (res) {
      if (res.error) return Template.message(res.error, res.alvo ? form.elements[res.alvo] : form.elements['_id'])
      Page.reload()
    })
  },

  remove: function () {
    $.delete('/api/sources', { source_id: Cache.source._id }, function (res) {
      if (res.error) return Template.message(res.error, '.modal-confirm .tingle-modal-box__content p')
      Modal._confirm.close()
      Modal._modal.close()
      Page.reload()
    })
  }

}
