var Page = {

  go: function (url) {
    location.href = url
  },

  title: function (str) {
    document.title = str
  },

  path: function (i) {
    if (!i) return location.pathname
    var parts = location.pathname.split('/')
    return parts[i] ? parts[i] : ''
  },

  goBack: function (e) {
    e && e.preventDefault()
    history.back()
  },

  reload: function (url) {
    location.reload()
  }

}
