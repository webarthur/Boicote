var Post = {

  join: function (_id, leave) {
    $.post('/api/posts/join', { post_id: _id, leave: leave }, function (res) {
      if (res.error) return Template.message(res.error, '.modal-confirm .tingle-modal-box__content p')
      if (leave) {
        $('#btnJoin [type=boicotar]').show()
        $('#btnJoin [type=boicotando]').hide()
      }
      else {
        $('#btnJoin [type=boicotar]').hide()
        $('#btnJoin [type=boicotando]').show()
      }
    })
  },

  load: function () {
    var slug = Page.path(2)
    $.get('/api/topico/' + slug, function (post) {
      if (post.error) return Template.message(post.error)

      // atualiza título da página
      Page.title(post.title + ' – Boicote!')

      // usado para salvar fonte
      Cache.post_id = post._id

      // aplica markdown e remove script injection
      post.text = escapeHTML(post.text)
      post.text = marked(post.text)
      post.info = escapeHTML(post.info)
      post.info = marked(post.info)

      post.num_reasons = post.reasons && post.reasons.length || 0

      // adiciona indice
      var i = 0
      post.reasons.map(function (item) {
        item.indice = ++i
        return item
      })

      // adiciona usuário logado ao escopo
      post.user = User.data

      // renderiza template
      Template.render('topic', Template.scope(post))

      // caixa de comentário auto dimensionável
      autosize($get('commentBox'))

      // atualiza sidebar
      $find('#sidebar p').innerHTML = post.info

      // configura botão de boicote
      if (post.participates) {
        $('#btnJoin [type=boicotar]').hide()
        $('#btnJoin [type=boicotando]').show()
      }

      // configura menu de opções
      $('#btnEditPost').fu_popover({
        content:
        '<ul layout="flat" class="popover-menu">'
        + '  <li onclick="Page.go(location.href + \'/editar\')">Editar</li>'
        + '  <li>Copiar link</li>'
        + '  <li>Convidar pessoas</li>'
        + '  <li>Denunciar abuso</li>'
        + '  <li class="divisor"></li>'
        + '  <li>Remover publicação</li>'
        + '</ul>',
        placement: 'bottom',
        dismissable: true
      })

      // popula comentários
      Comment.load(post._id, 'postComments')

      if (location.hash === '#Discussion') {
        $('a[href="#Discussion"]').click()
      }
    })
  },

  save: function (form, e) {
    e.preventDefault()

    var data = Form.serialize(form)

    if (!data.title.trim()) return Template.message('Informe o tema.', form.elements.title)
    if (data.title.trim().length < 40) return Template.message('Defina melhor seu tema, o título está muito curto.', form.elements.title)
    if (!data.text.trim()) return Template.message('Descreva o tema a ser debatido.', form.elements.text)
    if (data.text.trim().length < 80) return Template.message('Descreva melhor o contexto do boicote, o texto está muito curto.', form.elements.text)
    if (!data.info.trim()) return Template.message('Descreva melhor como boicotar, o texto está muito curto.', form.elements.info)
    if (data.info.trim().length < 80) return Template.message('Descreva melhor como boicotar, o boicote está muito curto.', form.elements.info)
    if (!data.tags.trim()) return Template.message('Informe a quais tópicos está relacionado.', form.elements.tags)

    // A fuligem produzida num ambiente urbano pode contaminar o alimento produzido por hortas urbanas
    $.post('/api/posts', data, function (res) {
      if (res.error) return Template.message(res.error, res.alvo ? form.elements[res.alvo] : null)
      Page.go(res.url)
    })
  },

  vote: function (vote, btn) {
    if (!User.data._id) return Modal.alert('Você precisa estar logado!')

    var classList = btn.classList
    var btnSelected = btn.parentNode.querySelector('.selected')

    // gerencia classes dos botoes
    if (classList.contains('selected')) {
      classList.remove('selected')
      vote = 0
    }
    else {
      if (btnSelected) btnSelected.classList.remove('selected')
      classList.add('selected')
    }

    var data = {
      post_id: Cache.source._id,
      vote: vote
    }

    $.post('/api/votes', data, function (res) {
      if (res.error) return Modal.alert(res.error)
    })
  },

  edit: function () {
    var slug = Page.path(2)
    $.get('/api/topico/' + slug, function (data) {
      if (data.error) return Template.message(data.error)

      // popula formulário
      Form.populate('#formPost', data)

      // mostra formulário
      $('main').show()

      // escolhe o botão cadastrar/atualizar
      if (data._id) {
        $('.hideIfEdit').hide()
        $('.showIfEdit').show()
      }
    })
  },

  show: function (e, id) {
    $('.tab-content').hide()
    $('#tab' + id).show()

    var $menu = $(e.parentNode)
    $menu.find('a').removeClass('active')
    $menu.find('a[href="#' + id + '"]').addClass('active')
  }

}
