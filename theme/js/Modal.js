var Modal = {

  isMobile: function () {
    return false
  },

  alert: function (text) {
    alert(text)
  },

  open: function (contents, closeMethods, maxWidth) {
    var modal = new tingle.modal({  // eslint-disable-line
      closeMethods: closeMethods || ['overlay', 'button', 'escape'],
      onOpen: function () {
        document.body.style.overflow = 'hidden'
        document.documentElement.style.overflow = 'hidden'
        document.querySelector('.tingle-modal').style.overflow = 'auto'
      },
      onClose: function () {
        document.body.style.overflow = 'auto'
        document.documentElement.style.overflow = 'auto'
      }
    })
    modal.setContent(contents)
    modal.open()
    this._modal = modal

    if (maxWidth) {
      $find('.tingle-modal-box').style.maxWidth = maxWidth + 'px'
    }

    var $input = $find('.tingle-modal-box form input[type=text]')
    if ($input) $input.focus()
  },

  confirm: function (contents, buttons) {
    var modal = new tingle.modal({  // eslint-disable-line
      closeMethods: ['overlay', 'button', 'escape'],
      cssClass: ['modal-confirm'],
      onOpen: function () {
        document.body.style.overflow = 'hidden'
        document.documentElement.style.overflow = 'hidden'
        document.querySelector('.tingle-modal').style.overflow = 'auto'
      },
      onClose: function () {
        document.body.style.overflow = 'auto'
        document.documentElement.style.overflow = 'auto'
      }
    })

    if (!buttons) {
      buttons =
      '<div class="rBox modal-footer" text="right">'
      + '  <button onclick="Source.remove()">Remover</button> &nbsp;'
      + '  <button type="cancelar" onclick="Modal.closeConfirm()">Cancelar</button>'
      + '</div>'
    }

    modal.setContent('<p>' + contents + '</p>' + buttons)
    modal.open()
    this._confirm = modal
  },

  close: function (e) {
    e.preventDefault()
    this._modal.close()
  },

  closeConfirm: function (contents) {
    this._confirm.close()
  }

}
