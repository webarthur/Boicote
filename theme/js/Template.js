var Template = {

  data: {},
  tags: ['{', '}'],
  loading: '<p>Carregando...</p>',
  engine: null,
  triggers: [function (html) {
    return html.replace(/data-src=/g, 'src=')
  }],

  regExp: function () {
    return new RegExp(Template.tags[0] + '([^\\' + Template.tags[1][0] + ']*)' + Template.tags[1], 'g')
  },

  scan: function () {
    $('[data-template]').each(function (i, item) {
      var id = item.getAttribute('id')

      // guarda template
      Template.data[id] = item.innerHTML

      // remove tags de modais do body
      if (id.indexOf('modal') === 0) {
        item.parentNode.removeChild(item)
      }
    })
  },

  render: function (id, scope) {
    var target = document.getElementById(id)
    target.innerHTML = this.applyTriggers(this.engine.render(Template.data[id], scope))
    target.removeAttribute('hidden')
    return target
  },

  renderHTML: function (tpl, scope) {
    return this.applyTriggers(this.engine.render(tpl, scope))
  },

  applyTriggers: function (html) {
    for (var i = 0; i < this.triggers.length; i++) {
      html = this.triggers[i](html)
    }
    return html
  },

  message: function (text, target, className) {
    // esconde mensagens anteriores
    $('.message').remove()

    // se texto for vazio, apenas limpa mensagens já exibidas
    if (!text) return

    // se não houver elemento-alvo exibe mensagem no modal
    if (!target) return Modal.alert(text) // TODO Template.alert()

    // cria nova mensagem
    var m = $('<div style="display:none" class="message"></div>')

    // adiciona estilos
    if (className) m.addClass(className)

    // atualiza texto
    m.html(text)

    // insere no antes elemento-alvo html
    $(target).before(m)

    // exibe mensagem com efeito
    return m.slideDown()
  }

}
