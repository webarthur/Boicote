// métodos PUT/DELETE para jquery
$.each(['put', 'delete'], function (i, method) {
  $[method] = function (url, data, callback, type) {
    if ($.isFunction(data)) {
      type = type || callback
      callback = data
      data = undefined
    }
    return $.ajax({
      url: url,
      type: method,
      dataType: type,
      data: data,
      success: callback
    })
  }
})

// https://stackoverflow.com/questions/7641791/javascript-library-for-human-friendly-relative-date-formatting
function formatDate (date, type) {
  if (type === 'human') {
    var delta = Math.round((+new Date() - new Date(date)) / 1000)
    var minute = 60
    var hour = minute * 60
    var day = hour * 24
    // var week = day * 7
    var fuzzy = ''

    if (delta < 30) {
      fuzzy = 'agora'
    } else if (delta < minute) {
      fuzzy = delta + ' segundos'
    } else if (delta < 2 * minute) {
      fuzzy = 'um minuto atrás'
    } else if (delta < hour) {
      fuzzy = Math.floor(delta / minute) + ' minutos'
    } else if (Math.floor(delta / hour) === 1) {
      fuzzy = '1 hora'
    } else if (delta < day) {
      fuzzy = Math.floor(delta / hour) + ' horas'
    } else if (delta < day * 2) {
      fuzzy = 'ontem'
    } else {
      fuzzy = formatDate(date)
    }

    return fuzzy
  } else {
    var d = new Date(date)
    var dia = d.getDate()
    var mes = d.getMonth() + 1
    var ano = d.getFullYear()
    var h = d.getHours()
    var m = d.getMinutes()

    if (dia.toString().length === 1) dia = '0' + dia
    if (mes.toString().length === 1) mes = '0' + mes
    if (m.toString().length === 1) m = '0' + m
    if (h.toString().length === 1) h = '0' + h

    return dia + '/' + mes + '/' + ano + ' às ' + h + ':' + m
  }
}

function formatTags (tags) {
  var spanTag = '<span class="tag">'
  tags = tags.split(/ *?, *?/)

  if (tags.length) return spanTag + tags.join('</span> ' + spanTag) + '</span>'
}

function escapeHTML (s) {
  var chars = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;'
  }
  return s.replace(/([&<>'"])/g, function (c) {
    return chars[c]
  })
}

var $get = function (id) {
  return document.getElementById(id)
}

var $find = function (q) {
  return document.querySelector(q)
}
