var Cache = {}

$(function () {
  // armazena templates
  Template.scan()

  // define escopo padrão para Template
  Template.engine = Mustache

  // define escopo padrão para Template
  Template.scope = function (scope) {
    return Object.assign({
      // retorna tags formatadas
      formatedTags: function () {
        return formatTags(this.tags)
      },

      // retorna data formatada
      date: function () {
        return formatDate(this.created_at)
      },

      // verifica qual avatar exibir
      avatarURL: function () {
        var data = this.author ? this.author : this

        if (!data.avatar) return '/img/guest.png'

        var parts = data.avatar.split('://')
        var protocol = parts[0]

        if (protocol === 'facebook') return 'https://graph.facebook.com/' + parts[1] + '/picture?type=normal'
        else if (protocol === 'gravatar') return 'https://www.gravatar.com/avatar/' + parts[1]
        else if (/https?/.test(protocol)) return data.avatar
        else return '/img/guest.png'
      }

    }, scope)
  }

  // pega dados do usuário
  User.credentials()

  // executa uma função se estiver previamente definida
  if (typeof __init === 'function') __init()
})
