
<div id="modalLogin" data-template hidden>
  <h2 class="hideIfEdit" layout="no-top bottom-20">Acesso ao site:</h2>

  <div text="center">
    <button onclick="Page.go('/auth/facebook')">
      <i class="fa fa-facebook"></i>
      Conectar com Facebook
    </button>
  </div>

  <div class="line">
    <span layout="white">ou</span>
  </div>

  <form id="formLogin" layout="fix" onsubmit="Auth.login(this, event); return false">
    <p>
      <label text="small bold" for="email">E-mail:</label>
      <input type="text" name="email" layout="fluid" placeholder="Informe seu email">
    </p>
    <p>
      <label text="small bold" for="pass">Senha:</label>
      <input type="password" name="pass" layout="fluid" placeholder="******">
    </p>
    <!-- <p>
      <label text="small bold" for="lembrar">
        <input type="checkbox" name="lembrar" placeholder="******" value="1"> Lembrar-me
      </label>
    </p> -->
    <p>
      <button type="submit" layout="left">Login</button>
      <button type="cancelar" layout="right" onclick="Modal.close(event)">Cancelar</button>
    </p>
  </form>
</div>
