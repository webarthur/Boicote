<!DOCTYPE html>
<html>
<head>
  <title>[[APPNAME]] – [[APPDESC]]</title>
  [[METAS]]
</head>
<body>
[[HEADER]]

<div cols="8" style="padding-right:60px">

  <article id="topic" layout="margin-20 left" data-template hidden>

    <header layout="flex no-margin">
      <h1>{{title}}</h1>
      <div layout="no-shrink top-30">
        <i id="btnEditPost" class="material-icons">&#xE5D4;</i>
      </div>
    </header>

    <menu class="tabs">
      <a href="#Text" class="active" onclick="Post.show(this, 'Text')">Contexto</a>
      <a href="#Discussion" onclick="Post.show(this, 'Discussion')">Discussão</a>
    </menu>

    <div id="tabText" class="tab-content">
      <div class="box" layout="flex row items-center bottom-10">
        <div id="btnJoin" layout="grow">
          <button type="boicotar" onclick="Post.join('{{_id}}')">
            <i class="fa fa-fw fa-ban"></i>
            Boicotar!
          </button>
          <button type="boicotando" onclick="Post.join('{{_id}}', true)" onmouseover="this.innerHTML='<i class=\'fa fa-fw fa-eye\'></i> Deixar boicote...'" onmouseout="this.innerHTML = '<i class=\'fa fa-fw fa-eye\'></i> Boicotando'" hidden>
            <i class="fa fa-fw fa-eye"></i>
            Boicotando
          </button>
        </div>
        <div text="inline-block gray-50 small" layout="right-15">
          {{date}}
        </div>
        <div>
          <img text="middle" src="{{avatarURL}}" alt="{{author.name}}" layout="circle" width="25">
          <b text="small">{{author.name}}</b>
        </div>
      </div>

      <div layout="h-padding-15">
        <p>
          {{{text}}}
        </p>
        <p>
          {{{formatedTags}}}
        </p>
      </div>

      <div id="reasonsList">
        <div>
          <header text="white bold" layout="black padding-10 no-margin">
            {{num_reasons}} Motivos
          </header>
          <ul layout="flat" style="background: #eee">
            {{#reasons}}
            <li onclick="Source.modal('{{_id}}', 'reasons')" text="line-1.5" layout="padding-10 fix" style="border-bottom:1px solid rgba(0, 0, 0, .1)">
              <div layout="flex">
                <div text="2em bold black-30" layout="right-15">
                  {{indice}}
                </div>
                <div style="max-width: 90%">
                  <h3 layout="top-5 bottom-15">{{title}}</h3>
                  <div text=".9em line-1.5">{{text}}</div>
                  <p text=".7em blue break-word" layout="v-margin-15">
                    <i class="fa fa-external-link"></i>
                    {{source}}
                  </p>
                </div>
              </div>
            </li>
            {{/reasons}}
            {{^reasons}}
            <li text="line-1.5" layout="padding-10 fix">
              <p text=".8em center" layout="no-margin">
                Nenhuma fonte ainda.
              </p>
            </li>
            {{/reasons}}
            <li onclick="Source.modalForm('reasons')" text=".9em line-1.5" layout="padding-10 black-10 fix">
              <i class="material-icons">&#xE145;</i> Adicionar um motivo
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div id="tabDiscussion" class="tab-content" hidden>

      <ul id="postComments" data-template layout="flat">
        {{#comments}}
        <li id="comment_{{_id}}" text="line-1.5" layout="v-padding-10 fix" style="border-bottom:1px solid rgba(0, 0, 0, .1)">
          <div layout="left">
            <img src="{{avatarURL}}" alt="{{author.name}}" layout="circle" width="40">
          </div>
          <div layout="auto" style="padding-left:10px">
            <div layout="flex">
              <h3 text=".8em bold" layout="grow no-margin">
                {{author.name}} <span text="normal gray-30">@{{author.nick}} {{humanDate}}</span>
                {{btnRemoveComment}}
              </h3>
              <span class="btn-remove" onclick="Comment.removeIfConfim('{{_id}}')" layout="{{btnRemoveComment}}">&times;</span>
            </div>
            <div text=".8em" layout="no-margin">
              {{{comment}}}
            </div>
          </div>
        </li>
        {{/comments}}
      </ul>

      <div class="rBox" layout="flex items-center">
        <div layout="right-15">
          <img src="{{#user}}{{avatarURL}}{{/user}}" alt="{{user.name}}" layout="circle" width="40">
        </div>
        <textarea id="commentBox" rows="1" layout="grow" placeholder="Comente algo sobre o assunto..." onkeyup="Comment.updateSendButton(this)" onchange="Comment.updateSendButton(this)"></textarea>
        <div id="commentSendButton" layout="left-15" class="disable" onclick="Comment.save('{{_id}}', 'postComments')">
          <i class="material-icons">&#xE163;</i>
        </div>
      </div>

    </div>

  </article>

</div>

<aside id="sidebar" cols="4">
  <section layout="yellow">
    <h2 text="1.3em bold black-80" layout="top-10">Como boicotar?</h2>
    <p text=".9em"></p>
  </section>
</aside>

<script>
function __init () {
  Post.load()
}
</script>

[[MODAL_FONTE]]
[[MODAL_FONTE_FORM]]

[[FOOTER]]
