# Boicote!

Ação coletiva a favor da justiça!

### Antes de instalar:

É preciso instalar o Node versão 8 ou superior e também o docker.

### Configurações

As configurações devem ser feitas no arquivo config/package.json. Há um arquivo chamado config/package.sample.json que pode servir de base para configurarmos o sistema.

### Instalando o sistema:

```
npm install
```
### Rodando o banco:

```
docker-compose up
```

### Rodando o sistema:

```
npm run dev
```
### Dica

Você pode rodar o banco e o servidor em abas separadas para poder acompanhar a saída nos dois sistemas.

### Acesso

localhost:8080
