global.config = require('./index.js')

config.database(config).then(db => {
  require('mongorm').setDB(db)
  global.Users = require('../app/Users.js')
  global.Posts = require('../app/Posts.js')
  global.Votes = require('../app/Votes.js')
  global.Comments = require('../app/Comments.js')
  global.Supporters = require('../app/Supporters.js')
  global.Mail = require('../app/Mail.js')
  Mail.defaults = config.server.mail
  Mail.devMailing = config.server.devMailing
  Mail.save = config.server.saveMailing
})
