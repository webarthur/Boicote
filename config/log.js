const fs = require('fs')
const util = require('util')
const logFile = fs.createWriteStream('./server.log', { flags: 'a' })

// DEV
if (process.env.NODE_ENV === 'DEV') {
  const notifier = require('node-notifier')
  global.console.log = global.console.error = function () {
    const d = new Date().toISOString()
    const datetime = '[' + d.slice(0, 10) + ' ' + d.slice(11, 19) + '] '

    logFile.write(datetime + util.format.apply(null, arguments) + '\n')
    process.stdout.write(util.format.apply(null, arguments) + '\n')

    notifier.notify({
      'title': 'Server',
      'message': arguments[0]
    })
  }
  require('./dev.js')
}
else {
  global.console.log = global.console.error = function () {
    const d = new Date().toISOString()
    const datetime = '[' + d.slice(0, 10) + ' ' + d.slice(11, 19) + '] '
    logFile.write(datetime + util.format.apply(null, arguments) + '\n')
    process.stdout.write(util.format.apply(null, arguments) + '\n')
  }
}
