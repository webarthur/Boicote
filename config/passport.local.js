const passport = require('passport')
const purifix = require('purifix')
const LocalStrategy = require('passport-local').Strategy
const config = require('./settings.json')
const assert = require('assertjs')
const md5 = require('md5')

passport.use(new LocalStrategy(config.auth.local, async function (req, email, pw, done) {
  const Users = require('../app/Users.js')

  // reset session
  req.session.user = false

  // limpa
  const data = {
    email: purifix.string(email).trim(),
    pass: purifix.string(pw)
  }

  // valida
  assert(data)
  .the('email').isNotEmpty().or('Informe seu email.')
  .the('email').isEmail().or('E-mail inválido')
  .the('pass').isNotEmpty().or('Informe sua senha.')
  if (assert.hasError) return done(assert.error)

  // procura por usuário
  const user = await Users.findOne({ email: data.email }, ['name', 'nick', 'avatar', 'pass', 'email'])
  if (Users.hasError) return done(new Error('Ocorreu um erro no sistema.'))
  if (!user) return done(new Error('Usuário não encontrado.'))

  // senha incorreta
  if (!Users.verifyPassword(data.pass, user.pass)) {
    return done(new Error('Senha incorreta.'))
  }

  // atualiza session
  req.session.user = {
    _id: user._id,
    name: user.name,
    nick: user.nick,
    avatar: user.avatar ? user.avatar : 'gravatar://' + md5(user.email)
  }

  done(null, {})
}))

module.exports = {

  authenticate (req, res, next) {
    passport.authenticate('local', function (err, user, info, statusCode) {
      if (err) return res.fail(err.message)
      if (info) return res.fail(info)
      res.json({
        _id: user._id,
        nome: user.nome,
        nick: user.nick,
        avatar: user.avatar,
      })
    })(req, res, next)
  },

  logout (req, res) {
    req.session.user = false
    req.logout()
    res.json({ logout: 1 })
  },

  isAuthenticated (req, res, next) {
    if (!req.user || !req.user._id) return res.fail('Você precisa estar logado.')
    next()
  }
}
