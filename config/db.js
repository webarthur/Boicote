const MongoClient = require('mongodb').MongoClient

module.exports = {

  setup (config) {
    const mongoString = config.server.mongo

    return new Promise((resolve, reject) => {
      MongoClient.connect(mongoString, function (err, db) {
        if (err) {
          console.error(err)
          return reject(err)
        }

        global.db = db

        resolve(db)
      })
    })
  }

}
