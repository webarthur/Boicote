const config = require('./settings.json')

if (process.env.NODE_ENV === 'DEV' || global.DEV) {
  console.log('DEV')

  // para funcionar sem https no localhost
  config.server.session.secure = false

  // troca para url de acesso local
  config.server.mongo = config.server.dev.mongo

  // troca para porta local
  config.server.port = config.server.dev.port
}

require('./log.js')

process.on('unhandledRejection', (error, p) => {
  console.error('Unhandled Rejection at: Promise', p, 'reason:', error)
  console.error(error.stack)
})

process.on('uncaughtException', err => {
  console.error('Caught exception: ' + err)
  throw err
})

// adiciona funções de autenticação para o roteador
config.passport = {
  local: require('./passport.local.js'),
  facebook: require('./passport.facebook.js')
}

// função para configurar banco de dados
config.database = require('./db.js').setup

module.exports = config
