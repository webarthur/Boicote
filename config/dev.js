const fs = require('fs')
const config = require('../config/settings.json')
const readFileSync = file => fs.readFileSync(file, 'utf8')
const UglifyJS = require("uglify-js")
const pretty = require("pretty")

const renderAllJS = (event, filename) => {
  const opt = {
    compress: true,
    sourceMap: {
      filename: "all.js",
      url: "all.js.map"
    }
  }
  const alljs = UglifyJS.minify({
    './theme/js/Page.js': readFileSync('./theme/js/Page.js'),
    './theme/js/Form.js': readFileSync('./theme/js/Form.js'),
    './theme/js/Auth.js': readFileSync('./theme/js/Auth.js'),
    './theme/js/Template.js': readFileSync('./theme/js/Template.js'),
    './theme/js/User.js': readFileSync('./theme/js/User.js'),
    './theme/js/Source.js': readFileSync('./theme/js/Source.js'),
    './theme/js/Comment.js': readFileSync('./theme/js/Comment.js'),
    './theme/js/Modal.js': readFileSync('./theme/js/Modal.js'),
    './theme/js/Post.js': readFileSync('./theme/js/Post.js'),
    './theme/js/functions.js': readFileSync('./theme/js/functions.js'),
    './theme/js/init.js': readFileSync('./theme/js/init.js'),
  }, opt)
  fs.writeFile('./public/all.js', alljs.code)
  fs.writeFile('./public/all.js.map', alljs.map)
  console.log('all.js renderizado!')
}

const renderLibsJS = (event, filename) => {
  let libsjs = ''
  libsjs += readFileSync('./theme/lib/jquery-3.2.1.min.js')
  libsjs += readFileSync('./theme/lib/mustache-2.3.0.min.js')
  libsjs += readFileSync('./theme/lib/marked-0.3.6.min.js')
  libsjs += readFileSync('./theme/lib/tingle-0.11.0/tingle.min.js')
  libsjs += readFileSync('./theme/lib/popover-1.1.2.min.js')
  libsjs += readFileSync('./theme/lib/autosize-4.0.0.min.js')
  fs.writeFile('./public/libs.js', libsjs)
  console.log('libs.js renderizado!')
}

const renderTemplates = (event) => {
  const lista = ['index', 'topico', 'topico-form', 'perfil', 'perfil-form', 'termos', 'sobre']
  const partials = {
    appName: config.name,
    appDesc: config.description,
    header: readFileSync('./theme/_header.tpl'),
    metas: readFileSync('./theme/_metas.tpl'),
    footer: readFileSync('./theme/_footer.tpl'),
    modalSource: readFileSync('./theme/modal-fonte.tpl'),
    modalSourceForm: readFileSync('./theme/modal-form-fonte.tpl'),
    modalSignup: readFileSync('./theme/modal-cadastro.tpl'),
    modalLogin: readFileSync('./theme/modal-login.tpl')
  }
  for (let template of lista) {
    let html = readFileSync('./theme/' + template + '.tpl')
    html = html.replace('[[HEADER]]', partials.header)
    html = html.replace('[[METAS]]', partials.metas)
    html = html.replace('[[FOOTER]]', partials.footer)
    html = html.replace('[[MODAL_FONTE]]', partials.modalSource)
    html = html.replace('[[MODAL_FONTE_FORM]]', partials.modalSourceForm)
    html = html.replace('[[MODAL_CADASTRO]]', partials.modalSignup)
    html = html.replace('[[MODAL_LOGIN]]', partials.modalLogin)
    html = html.replace('[[APPNAME]]', partials.appName)
    html = html.replace('[[APPDESC]]', partials.appDesc)
    fs.writeFileSync('./public/' + template + '.html', pretty(html))
  }
  console.log('Templates renderizados!')
}

const watch = (path, opt, fn) => {
  var lock = false
  fs.watch(path, opt, function () {
    if (!lock) {
      lock = true
      fn()
      setTimeout(() => lock = false, 1000)
    }
  })
}

watch('./theme', { interval: 500 }, renderTemplates)
watch('./theme/lib', { interval: 500 }, renderLibsJS)
watch('./theme/js', { interval: 500 }, renderAllJS)
