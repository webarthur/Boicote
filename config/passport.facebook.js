const passport = require('passport')
const purifix = require('purifix')
const FacebookStrategy = require('passport-facebook').Strategy
const config = require('./settings.json')

passport.serializeUser((user, done) => done(null, user))
passport.deserializeUser((user, done) => done(null, user))

passport.use(new FacebookStrategy(config.auth.facebook, async function (req, accessToken, refreshToken, profile, done) {
  const Users = require('../app/Users.js')

  // reset session
  req.session.user = false

  const email = profile.email ? profile.email : profile.id + '@facebook.com'
  const name = profile.name.givenName + ' ' + profile.name.familyName

  // procura pelo usuário
  let user = await Users.findOne({$or: [
    {facebook_id: profile.id},
    {email: email}
  ]}, ['name', 'nick', 'avatar'])
  if (Users.hasError) return done(new Error('Erro ao buscar usuário.'))

  if (!user) {
    // insere usuário
		await Users.insertOne({
      email: email,
			name: name,
			facebook_id: profile.id,
			avatar: 'facebook://' + profile.id,
		})
    if (Users.hasError) return done(new Error('Erro ao cadastrar usuário no sitema.'))

    // procura pelo usuário
    user = await Users.findOne({facebook_id: profile.id}, ['aid', 'bcrypt_pass'])
    if (Users.hasError || !user) return done(new Error('Ocorreu um erro no sistema.'))
  }

  // atualiza session
  req.session.user = {
    _id: user._id,
    name: user.name,
    nick: user.nick,
    avatar: user.avatar ? user.avatar : 'facebook://' + profile.id,
  }

  // Log::add([
  // 	'type' => 'newFacebookUser',
  // 	'data' => json_encode($user)
  // ]);

  done(null, req.session.user)
}))

module.exports = {

  authenticate (req, res, next) {
    passport.authenticate('facebook', function (err, user, info, statusCode) {
      if (err) return res.fail(err.message)
      if (info) return res.fail(info)
      res.json(user)
    })(req, res)
  },

  callback (req, res, next) {
    return passport.authenticate('facebook', { failureRedirect: '/?autherror' })(req, res, next)
  },

  ok (req, res) {
    res.redirect('/')
  }

}
